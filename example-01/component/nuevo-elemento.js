class NuevoElemento extends HTMLElement {
  // Constructor de la clase del componente heredando de HTMLElement
  constructor() {
    super();
    const shadow = this.attachShadow({mode: 'open'});
    const div = document.createElement('div');
    div.textContent = 'Segunda Jornada de Informática - NovaUniversitas';
    shadow.appendChild(div);
  }
  // INIT: ciclo de vida del componente
  //Componente agregado al dom...loaded
  connectedCallback() {
    console.log('Load NuevoElementoWeb agregado');
  }
  // Cuando el componente es eliminado del dom
  disconnectedCallback() {
    console.log('Removiendo NuevoElementoWeb');
  }
  /**
   * especificando un método estático get observedAttributes dentro de
   * la clase del elemento personalizado, esto devolvería una matriz que
    * contenga los nombres de los atributos que deseamos observar... :3
   * */
  static get observedAttributes() {
    return [];
  }
  // se ejecuta cada vez que uno de los atributos del elemento sufre algún cambio
  attributeChangedCallback(name, oldValue, newValue) {
    console.log('Cambian propiedades', name, oldValue, newValue);
  }
}

customElements.define('nuevo-elemento', NuevoElemento);