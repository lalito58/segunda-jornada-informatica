class ImagenCaption extends HTMLElement {
    // Constructor de la clase del componente heredando de HTMLElement
    constructor() {
      super();
      this.path = "https://i.imgur.com/C6jREu9.png";
      this.caption = "Homer J. Simpson";
      // this.initView();
      console.log('Constructor');
      this.attachShadow({mode: 'open'}); // add shadow root
    }
    // INIT: ciclo de vida del componente
    //Componente agregado al dom...loaded
    connectedCallback() {
      console.log('Load NuevoElementoWeb agregado');
      this.initView()
    }
    // init view
    initView() {
      // let shadow = this.attachShadow({mode: 'open'}); // add shadow root
      let style = document.createElement('style');
      style.textContent = this.getStyle();

      // creamos un titulo con un encabezado
      let title = document.createElement('h3');
      title.textContent = "Imagen with Caption";

      // creamos div principal
      let div = document.createElement('div');
      div.className = 'main-container'

      this.shadowRoot.appendChild(title);
      // agregamos el style al shadow root
      this.shadowRoot.appendChild(style);
      // agregamos el div principal a nuestro shadow root
      this.shadowRoot.appendChild(div);

      this.createImg();

    }
    createImg() {
      console.log("Create imng ", this, this.shadowRoot.querySelector('div.main-container'))
      let div = this.shadowRoot.querySelector('div.main-container');
      if (div !== null) {
        div.innerHTML = "";
        // creamos un figure
        let figure = document.createElement('figure');
        let figCaption = document.createElement('figcaption');
        figCaption.textContent = this.caption;

        // creamos una imagen
        let img = document.createElement('img');
        img.src = this.path;
        
        figure.appendChild(img);
        figure.appendChild(figCaption);

        // agregamos el div de la img al div principal
        div.appendChild(figure);
        this.shadowRoot.appendChild(div);
      }
    }
    // retorna mis estilos
    getStyle() {
      return `
      .main-container {
        border:1px solid lightgray;
        padding: 5px;
        text-align: center;
      }
      `;
    } 
    // Cuando el componente es eliminado del dom
    disconnectedCallback() {
      console.log('Removiendo NuevoElementoWeb');
    }
    /**
     * especificando un método estático get observedAttributes dentro de
     * la clase del elemento personalizado, esto devolvería una matriz que
      * contenga los nombres de los atributos que deseamos observar... :3
     * */
    static get observedAttributes() {
      return ['path', 'caption'];
    }
    // se ejecuta cada vez que uno de los atributos del elemento sufre algún cambio
    attributeChangedCallback(prop, oldValue, newValue) {
      console.log('Cambian propiedades', prop, oldValue, newValue);
      if (prop === 'path') {
        console.log(this, "newValue ", newValue, " oldValue ", oldValue);
        this.path = newValue;
        this.createImg();
      } else if (prop === 'caption') {
        this.caption = newValue;
        this.createImg();
      }
      
    }
  }
  
  customElements.define('imagen-caption', ImagenCaption);